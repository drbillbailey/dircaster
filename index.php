<?
// DirCaster configuration file
require_once('config_inc.php');
?>

<html>
<head>
<?
print"<title>$titleTAG</title>"
?>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
</head>
<body>

<?
// Change the specific code in this file to match your feed...
// Need to include the code to read ID3 tags
// also reads lots of different formats
// we'll implement mp3, mpa (quicktime), asf (wma) and riff (wav/avi)
// alter the path to your getid3 directory location
require_once( $id3LibPath );

print"<h1>$titleTAG</h1>"
?>
<h2>Listening to our Podcasts</h2>
<p>
You can just click on the title of the Podcast below and your web browser should open the file in an appropriate audio player and play the Podcast.<br>
You can also use an external audio player such as iTunes to open and play the episode. <a href="https://www.apple.com/itunes/download/" target="_blank">Click here if you need to install a free copy of iTunes from Apple</a>.<br>
A high speed Internet connection is not required but a typical 30 minute Podcast can take up to 45 minutes to download using a 56K dial-up Internet connection. A high speed connection will download the same Podcast in less than 1 minute.</p>
<p>You can also create an automatic subscription to the <a href="#about">podcasts</a> or have the podcasts downloaded to your iPod automatically. Subscriptions require you to install some additional free software and you can <a href="#subscribe">read about it here</a>.</p>

<hr>
<h2><a name="subscribe"></a>Subscribe to our Podcasts</h2>
<p><a href="dircaster.php"><img src="podcast_logo_grey.png" width="96" height="44" border="0" align="absmiddle"></a>
<? $rootMP3URL = "http://" . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
$rootMP3URL =  substr($rootMP3URL, 0, strrpos ($rootMP3URL, "/"));
$feedurl = $rootMP3URL."/dircaster.php";
echo ("Copy this link for your podcast program --> <a href=\"".$feedurl."\">".$feedurl."</a>"); 
?>
</p>
<p><em>(i.e. Some programs like <a href='https://juicereceiver.sourceforge.net/#download' title='Download Juice, the cross-platform podcast receiver' target='_blank'><img src='juice_receiver.gif' width="80" height="21" alt='Download Juice, the cross-platform podcast receiver' border='0' /></a> will require you to copy and paste the Postcast link URL to add the feed.)</em></p>

<hr>
<p><a name="List"></a></p>
<? 
///////////////
/////////////
// Main Code
///////////
$rootMP3URL = "http://" . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
$rootMP3URL =  substr($rootMP3URL, 0, strrpos ($rootMP3URL, "/")); // Trim off script name itself

print"		<h2>$titleTAG</h2>\n";
print"		<h3>from: <a href='$linkTAG'>$linkTAG</a></h3>\n";
print"		<p><font size='4'>$descriptionTAG</font></p>\n";
print"		<p>&copy; $copyrightTAG</p>\n";
print"		<p>Contact the Podmaster: <a href='mailto:$webMasterTAG'>$webMasterTAG</a><br></p>\n";
print"      <table border='1' align='center' cellpadding='2'>\n<tr><th colspan='7'>$titleTAG</th></tr>\n";
print"      <tr><th>Series</th><th>Title</th><th>Duration</th><th>Speaker</th><th>Created</th><th>Filesize</th><th>Filetype</th></tr>";

$dirArray = getDir($mediaDir, $sftypes);	// Get a list of the current directory
$countDirArray = count($dirArray);
if ($countDirArray < $maxFeeds) {
    $arraySlice = $countDirArray;
} else {
    $arraySlice = $maxFeeds;
}
$slicedDirArray = array_slice($dirArray, 0, $arraySlice);
foreach ($slicedDirArray as $filename => $filedate) {
	$mp3file = new CMP3File;
	$mp3file->getid3 ($filename);
	echo("<tr>");
	// album tag
 	echo ("<td>$mp3file->album</td>\n");
	// title tag
	echo ("<td><b><a href=".$rootMP3URL."/". htmlentities(str_replace(" ", "%20", $filename)) .">".str_replace("_", " ", $mp3file->title)."</a></b></td>\n");
    // duration tag
    echo ("<td><div align=\"right\">$mp3file->duration</div></td>\n");
    // composer tag
    echo ("<td>$mp3file->artist</td>\n");
    // date
	$createDate = date("r",$filedate);
	$year = getdate();
	$year = $year['year']." ";
	$left = strpos($createDate, $year) + 4;
	$formatDate = substr ($createDate, 0, $left);
    echo ("<td><div align=\"right\">".$formatDate."</div></td>\n");
    // filesize
    echo ("<td><div align=\"right\">".human_filesize($filename)."</div></td>\n");
    // filetype
 	echo ("<td>$mp3file->mime_type</td>\n");
	print "</tr>\n";
}
print "</table>\n";
echo "<p>&nbsp;</p>\n";

// Functions and Classes
function human_filesize($filename, $decimals = 2) {
// return filesize in human readable format
    $bytes = filesize($filename);
    $factor = floor((strlen($bytes) - 1) / 3);
    if ($factor > 0) $sz = 'KMGT';
    return sprintf("%.{$decimals}f", $bytes / pow(1024, $factor)) . @$sz[$factor - 1] . 'B';
}

function stripJunk ($text) {
// Strip non-text characters
	for ($c=0; $c<strlen($text); $c++) {
		if (ord($text[$c]) >= 32 AND ord($text[$c]) <= 122)
			$outText.=$text[$c];
	}
	return $outText;
}

// read a text frame from the ID3 tag. V2.2 and 2.3 are supported
function readFrame ($frameTag, $header, $ver) {
	if ($ver == 3) {
		$lengthOffset = 6;
		$textOffset = 11;
	} 
	if ($ver == 2) {
		$lengthOffset = 4;
		$textOffset = 7;
	}
	
	// find the tag we're looking for
	$tagStart = strpos ($header, $frameTag) ;
	// this code only reads the first 256 bytes on larger frames
	$tagLength = ord (substr($header, $tagStart + 1 + $lengthOffset, 1));
	$textStart = $tagStart + $textOffset;
	if ($frameTag == "COMM" || $frameTag == "COM") {
		$textStart = $textStart + 4;
		$tagLength = $tagLength - 4;
		}
	$tagText = substr($header, $textStart, $tagLength - 1);
	return stripJunk($tagText);
}

class CMP3File {
    //properties
    var $title;
    var $artist;
    var $album;
    var $year;
    var $comment;
    var $genre;
	var $composer;
	var $copyright;
	var $mime_type;
	var $duration;

  function getid3 ($file) {
  // Initialize getID3 engine
	if (file_exists($file))
	{ //after verifying the file exists,
		$getID3 = new getID3;

		// Analyze file and store returned data in $ThisFileInfo
		$ThisFileInfo = $getID3->analyze($file);

		// Optional: copies data from all subarrays of [tags] into [comments] so
		// metadata is all available in one location for all tag formats
		// metainformation is always available under [tags] even if this is not called
		getid3_lib::CopyTagsToComments($ThisFileInfo);

		// Output desired information in whatever format you want
		// Note: all entries in [comments] or [tags] are arrays of strings
		// See structure.txt for information on what information is available where
		// or check out the output of /demos/demo.browse.php for a particular file
		// to see the full detail of what information is returned where in the array
		//echo @$ThisFileInfo['comments']['artist'][0]; // artist from any/all available tag formats

	$mim = @$ThisFileInfo['mime_type']; // artist from any/all available tag formats
	$dur = @$ThisFileInfo['playtime_string']; // play duration from any/all available tag formats
	switch (strrchr(strtolower($file), "."))
		{
		case ".mp3";
			$tit = @$ThisFileInfo['id3v2']['comments']['title'][0]; // artist from any/all available tag formats
			$alb = @$ThisFileInfo['id3v2']['comments']['album'][0]; // artist from any/all available tag formats
			$art = @$ThisFileInfo['id3v2']['comments']['artist'][0]; // artist from any/all available tag formats
			$com = @$ThisFileInfo['id3v2']['comments']['comment'][3]; // artist from any/all available tag formats
			$cmp = @$ThisFileInfo['id3v2']['comments']['composer'][0]; // artist from any/all available tag formats
			$gen = @$ThisFileInfo['id3v2']['comments']['genre'][0]; // artist from any/all available tag formats
			break;
		case ".m4a";
			$tit = @$ThisFileInfo['quicktime']['comments']['title'][0]; // artist from any/all available tag formats
			$alb = @$ThisFileInfo['quicktime']['comments']['album'][0]; // artist from any/all available tag formats
			$art = @$ThisFileInfo['quicktime']['comments']['artist'][0]; // artist from any/all available tag formats
			$com = @$ThisFileInfo['quicktime']['comments']['comment'][0]; // artist from any/all available tag formats
			$cmp = @$ThisFileInfo['quicktime']['comments']['writer'][0]; // artist from any/all available tag formats
			//	$gen = @$ThisFileInfo['quicktime']['comments']['genre'][0]; // artist from any/all available tag formats
			break;
		case ".m4b";
			$tit = @$ThisFileInfo['quicktime']['comments']['title'][0]; // artist from any/all available tag formats
			$alb = @$ThisFileInfo['quicktime']['comments']['album'][0]; // artist from any/all available tag formats
			$art = @$ThisFileInfo['quicktime']['comments']['artist'][0]; // artist from any/all available tag formats
			$com = @$ThisFileInfo['quicktime']['comments']['comment'][0]; // artist from any/all available tag formats
			$cmp = @$ThisFileInfo['quicktime']['comments']['writer'][0]; // artist from any/all available tag formats
			//	$gen = @$ThisFileInfo['quicktime']['comments']['genre'][0]; // artist from any/all available tag formats
			break;
		case ".mov";
			$tit = @$ThisFileInfo['quicktime']['comments']['title'][0]; // artist from any/all available tag formats
			$alb = @$ThisFileInfo['quicktime']['comments']['album'][0]; // artist from any/all available tag formats
			$art = @$ThisFileInfo['quicktime']['comments']['artist'][0]; // artist from any/all available tag formats
			$com = @$ThisFileInfo['quicktime']['comments']['comment'][0]; // artist from any/all available tag formats
			$cmp = @$ThisFileInfo['quicktime']['comments']['director'][0]; // artist from any/all available tag formats
			//	$gen = @$ThisFileInfo['quicktime']['comments']['genre'][0]; // artist from any/all available tag formats
			break;
		case ".asf";
			$tit = @$ThisFileInfo['asf']['comments']['title'][0]; // artist from any/all available tag formats
			$alb = @$ThisFileInfo['asf']['comments']['album'][0]; // artist from any/all available tag formats
			$art = @$ThisFileInfo['asf']['comments']['artist'][0]; // artist from any/all available tag formats
			$com = @$ThisFileInfo['asf']['comments']['comment'][0]; // artist from any/all available tag formats
			$cmp = @$ThisFileInfo['asf']['comments']['composer'][0]; // artist from any/all available tag formats
			$gen = @$ThisFileInfo['asf']['comments']['genre'][0]; // artist from any/all available tag formats
			break;
		case ".wma";
			$tit = @$ThisFileInfo['asf']['comments']['title'][0]; // artist from any/all available tag formats
			$alb = @$ThisFileInfo['asf']['comments']['album'][0]; // artist from any/all available tag formats
			$art = @$ThisFileInfo['asf']['comments']['artist'][0]; // artist from any/all available tag formats
			$com = @$ThisFileInfo['asf']['comments']['comment'][0]; // artist from any/all available tag formats
			$cmp = @$ThisFileInfo['asf']['comments']['composer'][0]; // artist from any/all available tag formats
			$gen = @$ThisFileInfo['asf']['comments']['genre'][0]; // artist from any/all available tag formats
			break;
		default;
			$tit = $file; // artist from any/all available tag formats
		}
	$this->title = $tit;
	$this->composer = $cmp;
	$this->album = $alb;
	$this->comment = $com;
	$this->copyright = $cmp;
	$this->artist = $art;
	$this->mime_type = $mim;
	$this->duration = $dur;
	return true;
	} else {
	return false; // file doesn't exist
	}
  }
}
function getDir($mp3Dir, $supported_file_types) {	
// Returns directory as array[file]=date in newest to oldest order

	$dirArray = array();
	$diskdir = "./$mp3Dir/";
	if (is_dir($diskdir)) {
		$dh = opendir($diskdir);
		while (($file = readdir($dh)) != false ) {
			if (filetype($diskdir . $file) == "file" && $file[0]  != ".") {
	            		$fext = strrchr(strtolower($file), ".");
				if (strpos ($supported_file_types, $fext) > 0) {
					$ftime = filemtime($mp3Dir."/".$file); 
#                    $dirArray[$file] = $ftime;
                    $dirArray[$mp3Dir."/".$file] = $ftime;
				}
			}
		}
		closedir($dh);
	}
	asort($dirArray);
	$dirArray = array_reverse($dirArray);
	return $dirArray;
}
?>

<hr>
<h2><a name="about"></a>About Podcasting</h2>
<p><strong>About podcasting<br>
</strong>Podcasting became popular in 2004 as a method of publishing sound files to the Internet, allowing users to subscribe to a feed and receive new audio files automatically. Podcasting is distinct from other types of audio content delivery because of its subscription model, which uses the RSS 2.0 file format. This technique has enabled independent producers to create self-published, syndicated &quot;radio&quot; shows, and has given broadcast radio programs a new distribution channel.</p>
<p><strong>Differences from traditional broadcasting</strong><br>
Unlike radio programs, which are generally listened to as they are broadcast, podcasts are transferred to the listener as a digital media file and are consumed at the listener's convenience, similar to a VCR playing back a pre-recorded TV show.</p>
<p>From the producer's perspective, podcasts cannot have live participation or immediately reach large audiences as quickly as radio can. However, podcasting allows individuals to easily transmit content worldwide without the need for expensive equipment or licenses, and is frequently used together with an online interactive bulletin board or blog.</p>

<hr>
<p><a href="#List">Back to the podcast list</a> </p>
<p>&nbsp;</p>
</body>
</html>
