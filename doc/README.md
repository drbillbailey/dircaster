# Installing and Setting Up DirCaster

## By: Dr. Bill Bailey

 You will need to download the latest version of **DirCaster** from the web site located at: [http://www.dircaster.org](http://www.dircaster.org) – the latest version will always be at the top of the download page, with older versions listed below that. At the time of this writing, the latest version was **DirCasterV09j**. Select the “ **Downloads** ” page from the **Site Index** , at the left of the page:

![screenshot](images/images-000.png)

You will then see the file at the top of the “ **Downloads** ” page.

![screenshot](images/images-001.jpg)

“ **Right-Click** ” on this link to get a menu that will allow you to download the file.

![screenshot](images/images-002.png)

Once you have downloaded the file (it will probably be located in your “ **Downloads** ” folder, by default,) open the file to extract it. (You may also use an “ **unzip** ” program like WinZip , or [7zip](https://www.7-zip.org/) to extract the files.)

![screenshot](images/images-003.png)

![screenshot](images/images-004.png)

Within the DirCasterV09j directory (that is, the directory name in this version,) you will find a second ” **DirCasterV09j** ” sub-directory, if you extracted it with Windows as shown previously. Change the name of this directory to whatever you would like to use to describe what this will be on your web site. In this example, I chose “ **video** .” So that the URL of our RSS feed would be http://www.yoursite.org/video/dircaster.php (Note: “ **yoursite.org** ” would, of course, be whatever your **web site domain** is.)

![screenshot](images/images-005.png)

We will be copying this **entire** directory up to your web server. You will have to secure the FTP **username** and **password** of the FTP site for your web server, and web directory, from your web hosting company. This is usually provided so that you can update your site, and maintain it.

Your web hosting provider **should** also provide **PHP** services on your web site. This is typically available whether your web hosting company uses Linux or Windows based servers to provide your web site’s hosting “back-end.”

**DirCaster** is written in **PHP**, and **DirCaster WILL NOT FUNCTION** if you do not have **PHP** service on your site. As I say, this is a very strong common standard, and will most likely be available.

We will be editing a file in the directory that we renamed as “ **video** ” called “ **config_inc.php** ”.

![screenshot](images/images-006.png)

This file is the configuration file for **DirCaster**, it contains **ALL** the information that you will need to add, or modify, to set up **DirCaster**. You **MUST** edit this file to set up **DirCaster**. In fact, reading the options in this file will give you a good overview of **DirCaster’s** many configuration options. **_The biggest mistake made in setting up DirCaster is NOT setting up this file properly!_**

A fairly new feature of **DirCaster** is the ability to use what we call “ **Remote Media** .” This is also known as “ **The Cloud Option** .” I use this to store large video files in the Amazon S3 Cloud, because I get faster viewing performance, and the integration of **CloudFront** in Amazon S3, allows video files to play from remote, yet close to the user, locations around the world. I **STRONGLY** suggest using this option for video. If you are using audio files (such as MP3 files,) they can be stored on your web server in the DirCaster directory, and you would leave the **`$remoteMedia`** option in **config_inc.php** set to “ **0** ” (which is the default.) However, if you do choose to use the “ **Cloud Option** ” and store the files in another location, or in the Amazon S3 cloud, as I do, you will need to set the **`$remoteMedia`** option to “ **1** ” (no quotes.) This is shown below:

![screenshot](images/images-007.png)

**THIS IS CRITICAL!** This option must be selected to use the “ **Cloud Option!** ” The location of the media will be set in the “ override ” file that corresponds to the media file that is stored in a different location.

Continue to edit, and fill out, the information in the “ config_inc.php ” file until it matches your site, and RSS feed information. Now, let’s move the file up to your web server!

You will need to download, and install, an FTP client. I can recommend, the Open Source, and therefore, free FileZilla FTP Client. It is available at: [https://filezilla-project.org](https://filezilla-project.org)

![screenshot](images/images-008.png)

Click on the “ Download FileZilla Client – All Platforms ” icon to download the file.

![screenshot](images/images-009.png)

You may then click on the executable to launch the set-up procedure, and click “ I Agree ” to proceed.

Pay close attention to the prompts that appear during the installation. Install the options that you desire.

Click “ **Next** ”
![screenshot](images/images-010.png)

Click “ **Next** ”
![screenshot](images/images-011.png)

Click “ **Next** ”
![screenshot](images/images-012.png)

Click “ **Install** ”
![screenshot](images/images-013.png)

Click “ **Finish** ”
![screenshot](images/images-014.png)

At this point, FileZilla will open. Now, you will want to keep the login credentials for your web site’s FTP access handy, and let’s set up the connection in FileZilla. You will need (as I mentioned before) your username and password to your web site FTP access.

![screenshot](images/images-015.png)

Click “ **File** ” then “ **Site Manager** .”

![screenshot](images/images-017.png)

Click “ **New Site** ”

![screenshot](images/images-018.png)

Fill in the “ **New Site** ” name as desired.

![screenshot](images/images-020.png)

Fill out the “ **Host** ” (usually the site domain name,) change “ **Logon Type** ” to “ **Normal** ,” and fill in the “ **User** ” and “ **Password** .” Click “ **OK** ” to save it in your **Site Manager**.

Now, you can access your web site files by opening your Site Manager and clicking on the Site in the list that you created.

There are two panes one on the left, which is your local PC, and the other, on the right, which is your web site.

Since we have expanded all the files into a directory that we called “ **video** ” we can now simply drag that directory from the left to the right pane.

When you do this, you will see the files copy over to the web server.

![screenshot](images/images-022.png)

You may “ **drag and drop** ” or, as shown above, you may “ **right-click** ” on the directory and select the “ **Upload** ” option.

![screenshot](images/images-024.png)

The files will transfer to the web server, and when complete, your **RSS Feed Site** will be ready!

If you access your site now with a browser that properly displays RSS feeds (like Mozilla Firefox) you may see this:

![screenshot](images/images-026.png)

“ **Your Title** ” and “ **Your Description** ” is part of the information that **should** have been edited in the “ **config_inc.p** hp” file in the **DirCaster** directory.

Note the missing image at the right hand upper section of the screen. There is no specification of an image file, so only the “ **broken link** ” image is seen.

On the next page is an example of a correctly filling in config file and RSS feed from my radio broadcast link: **[http://broadcast.wofm.org](http://broadcast.wofm.org)**.

![screenshot](images/images-028.png)


Note that this is a “fully – formed” page, with links, file specific information, a correct image for the RSS feed, and descriptive information, all of which is pulled from the “ **config_inc.php** ” file.

Note also that the individual, **file-specific** information is being pulled from the metatags in each audio (MP3) file. Metatags are added to the audio files by a metatag editor like the free, **MP3Tag**. You may download and install **MP3Tag** from: **[http://mp3tag.de/en/](http://mp3tag.de/en/)**

Next, we will see how to properly fill out the MP3 metatag information using **MP3Tag**.

Once you download it to your local PC, you can install it by “ **double-clicking** ” on the file. Just accept the defaults. The image below is from a document I created concerning using MP3Tag with **WOFR.org –Word of Faith Radio**. Also note, that the size of the image file will vary by application. 300x300 pixels is fine for most applications, however, **_iTunes is now requiring 1400x1400 pixel image files!_** iTunes also prefers .jpg extension (jpeg format) files to the Portable Network Graphic (PNG) files such as in the image below.

Once you install it, you can launch MP3Tag by selecting, “ **Start** ” → “ **All Programs** ” → “ **MP3tag** ,” and then clicking on the program. You will then see a screen like the one below:

![screenshot](images/images-030.png)

You may open a directory of MP3 files by clicking on “ **File** ” (upper left menu) and then select “ **Change Directory** ” from the menu. Select the directory where your MP3 audio files are, and they will be displayed in the right (white area) screen of the program.
Click once ( **not** a “ **double-click** ”) to select a file. This is in the white window, for instance, in the screen shot above, the selected file is: **WoFNetcast011110A.mp3**
Once the file is “highlighted” (as shown in the screen shot) you may fill in the information as shown.

**PLEASE NOTE:** Nothing will be saved until you click the small “floppy” symbol (“ **Save** ”) in the upper left corner! I mention this because it can be **very frustrating** to fill everything out and go to the next file, and then lose all you have typed... hit “ **Save** ” often! It doesn't hurt, and it can save you some frustration!

**_YOU DO NOT HAVE TO FILL OUT ALL FIELDS!_** The important ones are: “ **Artist** ” (Speaker) and “ **Title** ,” all other fields may be left blank as you wish.

**_Here's a neat tip!_** If you have a lot of files listed in the white window, and you click once on the top file, then hold down “ **Shift** ” and click on the last file, you will select **ALL** the files! Then, you may type something in one field, let's say the “ **Artist** ” field, and click “Save.” Then, all your files will be tagged with just that information! This is helpful when tagging a lot of files! Then you can go back and fill in just the “ **Title** ” field, for instance, on each file, **WITHOUT** having to type in the “ **Artist** ” field again and again on every file! This speeds things up a lot!

Also, **NEVER** tag a file while it is playing! You run the risk of corrupting the file's audio content. Wait until you have them as you want them and they are just sitting in the PC directory to which you have saved them.

Keep in mind that for **DirCaster** to work properly in “local mode” ( **non-remote media** ) usage, you **MUST** have the MP3 Audio file’s metatag information filled in, and, it is just more professional to do so.  The beauty of **DirCaster** as an application is **DirCaster’s** ability to “read” MP3 metatag data from a file, and create your RSS XML file automatically! It is vastly superior to “hand-coding” an XML coded RSS feed!

Once **DirCaster** is correctly set up, and if you use it for audio files, you may not need to do anything but copy your MP3 audio files into your **DirCaster** directory, from then on! **DirCaster** will **_auto-magic-ally_** create your RSS feed “on the fly!”

Now, let’s move on to the use of the “ **Remote Option** ” or, as we call it, the “ **Cloud Option** !” This discussion begins on the next page.


# “The Cloud Option!”

This, as I have mentioned before, is a relatively new, but **very** popular, feature of **DirCaster**. It involves superseding one of **DirCaster** ’s most powerful features, however. That feature is the ability of the code to “ **read** ” the MP3 metatag that we discussed just prior to this page. That feature allows the software to “ **read** ” the metatags, then “ **write** ” the information, in the proper format, into an iTunes compliant RSS feed.

Originally, this was done to allow audio podcasts, in the form of MP3 files, to be dropped into the
**DirCaster** directory on your web server, then, the **DirCaster** directory would display the properly formatted XML code for an RSS feed. This greatly speeds up production of audio podcasts, and relieves you from hand coding RSS feeds.

However, video files, such as MP4s, while they can have metatags added, are not properly read by **DirCaster** due to being structured differently than a standard MP3 audio file. Also, video files are usually very large, and may **not** be delivered well by a normal web server. It is advantageous to move the video files to a Cloud Based location, due to these space, and speed, considerations.

In fact, **Amazon S3** , with the **Cloudfront** feature enabled, provides exceptional **Video on Demand** (VOD) delivery of video via the web! This is due to the fact that Amazon optimizes the delivery speed, and Cloudfront “moves” a copy of the file to locations around the world, making the file requested appear more “local” to the user, depending on their location. Also, with your content dispersed around the globe, you have less impact on the specific media location. In other words, it “ **spreads out** ” your file “ **exposure** ” and file access.

In order to do this, we need to take care of entering the metadata for each file (usually video files, but
you could also use this feature to store audio files on another server, service, or in the Cloud.) This involves setting the all important, “switch” in the **config_inc.php** file as mentioned **previously**

Once this is set, you can use the “ **Override System** ” to enter the metadata, and **DirCaster** will use this information to “auto-magic-ally” generate the RSS feed from the data provided that is written into flat text files that are written into a directory called “ **override** ” which exists as a sub-directory “below” the main **DirCaster** directory.

You can name these files whatever you would like to keep up with the files, and their contents, I suggest
a numbering system, or the date of the program. For instance, if the video was recorded on April 17,
2015, you might name the file **2015-APR-17.txt** – let’s look at the screen on the next page that allows you to enter the information.

![screenshot](images/images-031.jpg)

Enter the override file name in the field shown above by the “red arrow.”

![screenshot](images/images-032.jpg)

Then select an existing template from the drop down list to have a pre-filled in file to save time.

![screenshot](images/images-034.png)

Click on the button to proceed.

![screenshot](images/images-036.jpg)

It will turn red to indicate that it is ready to write the file. Click on the link to proceed.

![screenshot](images/images-038.png)

Now, you can edit and change the data as needed from the template to the information appropriate to your file. You may use the drop down to select the file type.

![screenshot](images/images-039.jpg)

Now you may scroll down the web page and select “ **Save** ” to save your file.
Keep in mind, that the moment you click “ **Save** ” and then confirm it, your file **WILL** be written to your web directory, and your RSS Feed will be “ **Live** ” with that information, so you will want to be sure that your file is in place, and that the metadata was previously written into the file using a tool like **MP3Tag**.

Also, note the additional links and tools shown on the **Override System** page. You may select validating your RSS feed via **FeedValidator** , and you may select “ **Test feed in browser** ” (be sure that you are using a browser that **properly** displays RSS Feeds, such as **Mozilla Firefox** .) Otherwise, you will see the actual XML code of the feed, not the feed result. There is also a link to the official **Apple iTunes Reference** page, and link to **DirCaster.org** and a **Time Zone Table** that will give you the time based on any time zone.

Properly constructed RSS Feeds are the key for many successful projects, whether it is MP3-based, or other format, audio feeds, for podcasts; RSS Feeds for Video Netcasts, Video-On-Demand, or even RSS Feeds for “Over-the-Top TV” (OTT) set-top systems like the Roku.

**I trust that you will find DirCaster simple, flexible, and useful!**

**Dr. Bill Bailey, Maintainer
The DirCaster Open Source Project
[http://www.DirCaster.org](http://www.DirCaster.org)**
